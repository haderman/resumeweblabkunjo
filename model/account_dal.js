var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO account (email) VALUES (?)';

    var queryData = [params.email];

    connection.query(query, params.email, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var account = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account (email, first_name, last_name) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountData = [];
        if (params.email.constructor === Array) {
            for (var i = 0; i < params.email.length; i++) {
                accountData.push([account, params.email[i]]);
            }
        }
        else {
            accountData.push([account, params.email]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [accountData], function(err, result){
            callback(err, result);
        });
    });

};