var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal');

// View ALL Addresses
router.get('/all', function(req,res) {
    address_dal.getAll(function (err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('address/addressViewAll', {'result':result});
        }
    })
});

// View the address for the given id
router.get('/', function(req, res){
    if(req.query.address_id == null) {
        res.send('address_id is null');
    }
    else {
        address_dal.getById(req.query.address_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('address/addressViewById', {'result': result});
            }
        });
    }
});

// Return the add a new address form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Address/addressAdd', {'address': result});
        }
    });
});

// insert the address for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Address_id must be provided.');
    }
    else if(req.query.street == null) {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        address_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Address/all');
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.street == null) {
        res.send('street is null');
    }
    else {
        address_dal.delete(req.query.street, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Address/all');
            }
        });
    }
});


module.exports = router;