var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');

// View ALL account
router.get('/all', function(req,res) {
    account_dal.getAll(function (err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', {'result':result});
        }
    })
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Account/accountAdd', {'account': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('email must be provided.');
    }
    else if(req.query.email == null) {
        res.send('At least one email must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Account/all');
            }
        });
    }
});

module.exports = router;